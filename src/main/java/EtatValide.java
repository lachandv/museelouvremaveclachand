/***********************************************************************
 * Module:  EtatValide.java
 * Author:  Flow
 * Purpose: Defines the Class EtatValide
 ***********************************************************************/

/** @pdOid 58654871-c9d8-4ff4-9b13-3eb5da828829 */
public class EtatValide implements Etat {

    private Demande demande;

    public EtatValide(Demande d){
        this.demande = d;
    }
    @Override
    public void valider() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void prendre() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void moderer() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void abandonner() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }
}