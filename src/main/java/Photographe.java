/***********************************************************************
 * Module:  Photographe.java
 * Author:  Flow
 * Purpose: Defines the Class Photographe
 ***********************************************************************/

import java.util.*;

/** @pdOid 0a54f342-f442-40ad-a74e-a5f16fa6efea */
public class Photographe extends Personnel {

   public Photographe(String nom, String prenom, String mdp, Systeme syst) {
		super(nom, prenom, mdp, syst);
		// TODO Auto-generated constructor stub
	}
   
   /** @pdOid 239f38f0-2fbc-471c-9cfa-dd5b4273cc44 */
   public ArrayList<String> consulterDemandesAttente() {
	  return syst.ConsulterDemandesEnAttente();
   }
   
   /** @pdOid 5ef170b1-9bb8-4c3f-88c0-ffb9ec1a0518 */
   public ArrayList<String> consulterDemandesPrises() {
      return syst.ConsulterDemandesPrises(this);
   }
   
   /** @pdOid a3111668-1c6b-4293-8971-d12bdf71e76a */
   public void prendreDemande(int id) {

	   syst.prendreDemandes(this, id);

   }

   public void abandonerDemande(int id) {
      syst.abandonerDemande(id);
   }
}