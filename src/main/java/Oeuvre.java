/***********************************************************************
 * Module:  Oeuvre.java
 * Author:  Flow
 * Purpose: Defines the Class Oeuvre
 ***********************************************************************/

import java.util.*;

/** @pdOid 3ea7434f-8747-44d5-8e50-14f0365f1dba */
public class Oeuvre {
   /** @pdOid df5232e2-8b79-47a4-b799-e1a488be1634 */
   private String Description;
   /** @pdOid ad134052-b124-4b82-8250-49bf8734236b */
   private String Nom;
   
   /** @pdRoleInfo migr=no name=Photo assc=Association_1 coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Photo> photo;
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<Photo> getPhoto() {
      if (photo == null)
         photo = new java.util.HashSet<Photo>();
      return photo;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorPhoto() {
      if (photo == null)
         photo = new java.util.HashSet<Photo>();
      return photo.iterator();
   }

    public String getNom(){
	return this.Nom;
    }

    public String getDescription(){
	return this.Description;
	    }
   
   /** @pdGenerated default setter
     * @param newPhoto */
   public void setPhoto(java.util.Collection<Photo> newPhoto) {
      removeAllPhoto();
      for (java.util.Iterator iter = newPhoto.iterator(); iter.hasNext();)
         addPhoto((Photo)iter.next());
   }

    public void setDescription(String str){
	this.Description = str;
    }

  public void setNom(String str){
      this.Nom = str;
	    }
   
   /** @pdGenerated default add
     * @param newPhoto */
   public void addPhoto(Photo newPhoto) {
      if (newPhoto == null)
         return;
      if (this.photo == null)
         this.photo = new java.util.HashSet<Photo>();
      if (!this.photo.contains(newPhoto))
         this.photo.add(newPhoto);
   }
   
   /** @pdGenerated default remove
     * @param oldPhoto */
   public void removePhoto(Photo oldPhoto) {
      if (oldPhoto == null)
         return;
      if (this.photo != null)
         if (this.photo.contains(oldPhoto))
            this.photo.remove(oldPhoto);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllPhoto() {
      if (photo != null)
         photo.clear();
   }

}
