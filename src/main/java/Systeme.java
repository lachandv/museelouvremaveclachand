/***********************************************************************
 * Module:  Systeme.java
 * Author:  Flow
 * Purpose: Defines the Class Systeme
 ***********************************************************************/

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/** @pdOid 6b53637d-a479-4329-a458-3cd66f512797 */
public class Systeme {
	
	public Collection<Historien> historien;
	public Collection<Oeuvre> oeuvre;
	public Collection<Demande> demande;
	public Collection<Photographe> photographe;

	/** @pdOid 45d33383-00fa-4d0a-9095-1945b78496bb */
	public Oeuvre RechercherFiche(String nom) {
		int cpt = 0;
		Object[] tabOeuvre = new Object[oeuvre.size()];
		tabOeuvre = oeuvre.toArray();
		while (cpt < tabOeuvre.length){
			if (((Oeuvre) tabOeuvre[cpt]).getNom().toLowerCase().equals(nom.toLowerCase())){
				return (Oeuvre) tabOeuvre[cpt];
			}
			else cpt = cpt+1;
		}
		return null;
	}

	/** @pdOid 593d3a96-96eb-4089-9e5e-14ad446999d5 */
	public String ConsulterFiche(String nom) {
		String resultat = new String();
		Oeuvre oeuvre = RechercherFiche(nom);
		if (oeuvre != null){
			resultat=("Nom :" + oeuvre.getNom() + "\n" +
					"Description : " + oeuvre.getDescription());
		}
		else{
			resultat = "L'oeuvre n'existe pas";
		}
		
		return resultat;
	}
	
	public ArrayList<String> ConsulterDemandesParHistorien(Historien henri){
		ArrayList<String> resultat = new ArrayList<String>();
		Historien histoTmp;
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			histoTmp = tmp.getHistorien();
			if (histoTmp == henri){
			resultat.add("Identifiant : " + tmp.getIdentifiant() + "\n" +
			"Description demande : " + tmp.getDescription() + 
			"\n  Etat : " + tmp.getEtat());
			}
		}
		return resultat;
	}
	
	public ArrayList<String> ConsulterDemandesEnAttente(){
		ArrayList<String> resultat = new ArrayList<String>();
		Collection<Demande> demandeSyst = this.getDemande();
		Demande tmp;
		Demande demandeTmp;
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for (int i = 0; i< demandeSyst.size(); i++){
			tmp = iterateur.next();
				if (tmp.getEtat().getClass().equals(EtatEnAttente.class)){
					resultat.add("Identifiant : "+ tmp.getIdentifiant() +
							"\n" + "Description : " + tmp.getDescription()+ 
							"\n  Etat : " + tmp.getEtat());
				}
		}
		return resultat;
	}
	
	public ArrayList<String> ConsulterDemandesPrises(Photographe phot){
		ArrayList<String> resultat = new ArrayList<String>();
		Photographe photTmp;
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			photTmp = tmp.getPhotographe();
			if (photTmp == phot){
			resultat.add("Identifiant : " + tmp.getIdentifiant() + "\n" +
			"Description demande : " + tmp.getDescription()+ 
			"\n  Etat : " + tmp.getEtat());
			}
		}
		return resultat;
	}
	
	public int faireDemande(String descr, Oeuvre oeuvre, Historien historien){
		Demande tmp = new Demande(descr, oeuvre, historien);
		this.addDemande(tmp);
		return tmp.getIdentifiant();
	}


	/** @pdGenerated default getter */
	public java.util.Collection<Oeuvre> getOeuvre() {
		if (oeuvre == null)
			oeuvre = new java.util.HashSet<Oeuvre>();
		return oeuvre;
	}
	
	/** @pdGenerated default getter */
	public java.util.Collection<Demande> getDemande() {
		if (demande == null)
			demande = new java.util.HashSet<Demande>();
		return demande;
	}
	
	/** @pdGenerated default getter */
	public java.util.Collection<Historien> getHistorien() {
		if (historien == null)
			historien = new java.util.HashSet<Historien>();
		return historien;
	}
	
	/** @pdGenerated default getter */
	public java.util.Collection<Photographe> getPhotographe() {
		if (photographe == null)
			photographe = new java.util.HashSet<Photographe>();
		return photographe;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator getIteratorOeuvre() {
		if (oeuvre == null)
			oeuvre = new java.util.HashSet<Oeuvre>();
		return oeuvre.iterator();
	}

	/** @pdGenerated default setter
	 * @param newOeuvre */
	public void setOeuvre(java.util.Collection<Oeuvre> newOeuvre) {
		removeAllOeuvre();
		for (java.util.Iterator iter = newOeuvre.iterator(); iter.hasNext();)
			addOeuvre((Oeuvre)iter.next());
	}

	/** @pdGenerated default add
	 * @param newOeuvre */
	public void addOeuvre(Oeuvre newOeuvre) {
		if (newOeuvre == null)
			return;
		if (this.oeuvre == null)
			this.oeuvre = new java.util.HashSet<Oeuvre>();
		if (!this.oeuvre.contains(newOeuvre))
			this.oeuvre.add(newOeuvre);
	}
	
	/** @pdGenerated default add
	 * @param newDemande */
	public void addDemande(Demande newDemande) {
		if (newDemande == null)
			return;
		if (this.demande == null)
			this.demande = new java.util.HashSet<Demande>();
		if (!this.demande.contains(newDemande))
			this.demande.add(newDemande);
	}

	/** @pdGenerated default remove
	 * @param oldOeuvre */
	public void removeOeuvre(Oeuvre oldOeuvre) {
		if (oldOeuvre == null)
			return;
		if (this.oeuvre != null)
			if (this.oeuvre.contains(oldOeuvre))
				this.oeuvre.remove(oldOeuvre);
	}

	/** @pdGenerated default removeAll */
	public void removeAllOeuvre() {
		if (oeuvre != null)
			oeuvre.clear();
	}

	public void prendreDemandes(Photographe photographe2, int id) {
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getIdentifiant() == id){
				try{
					tmp.getEtat().prendre();
					tmp.setPhotographe(photographe2);}
				catch(IllegalStateException e){
					System.out.println("Action impossible dans cet état");
				}
			}
		}
		
	}

	public void validerDemande(int id, boolean validation) {
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getIdentifiant() == id){
				try {
					tmp.getEtat().valider();
				}
				catch(IllegalStateException e){
					System.out.println("Action impossible dans cet état");
				}
			}
		}

	}

	public void modererDemande(String message, int id) {
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getIdentifiant() == id){
				try{
					tmp.getEtat().moderer();
					tmp.setModeration(message);}
				catch(IllegalStateException e){
					System.out.println("Action impossible dans cet état");
				}
			}
		}

	}

	public void abandonerDemande(int id) {
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getIdentifiant() == id){
				try{
					tmp.getEtat().abandonner();
					tmp.setPhotographe(null);}
				catch(IllegalStateException e){
					System.out.println("Action impossible dans cet état");
				}
			}
		}

	}

	public ArrayList<String> consulterDemandesEnAttente(){
		ArrayList<String> resultat = new ArrayList<String>();
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getEtat().getClass().equals(EtatEnAttente.class)){
				System.out.println("Ok");
				resultat.add("Identifiant : " + tmp.getIdentifiant() + "\n" +
						"Description demande : " + tmp.getDescription() +
						"\n  Etat : " + tmp.getEtat());
			}
		}
		return resultat;
	}

	public ArrayList<String> consulterDemandesEnModeration(){
		ArrayList<String> resultat = new ArrayList<String>();
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getEtat().getClass().equals(EtatEnModeration.class)){
				resultat.add("Identifiant : " + tmp.getIdentifiant() + "\n" +
						"Description demande : " + tmp.getDescription() +
						"\n  Etat : " + tmp.getEtat());
			}
		}
		return resultat;
	}

	public ArrayList<String> consulterDemandesPrisEnCharge(){
		ArrayList<String> resultat = new ArrayList<String>();
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getEtat().getClass().equals(EtatPrisEnCharge.class)){
				resultat.add("Identifiant : " + tmp.getIdentifiant() + "\n" +
						"Description demande : " + tmp.getDescription() +
						"\n  Etat : " + tmp.getEtat());
			}
		}
		return resultat;
	}

	public ArrayList<String> consulterDemandesValide(){
		ArrayList<String> resultat = new ArrayList<String>();
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			if (tmp.getEtat().getClass().equals(EtatValide.class)){
				resultat.add("Identifiant : " + tmp.getIdentifiant() + "\n" +
						"Description demande : " + tmp.getDescription() +
						"\n  Etat : " + tmp.getEtat());
			}
		}
		return resultat;
	}

	public void addHistorien(Historien henri) {
		if (this.historien == null) {
			this.historien = new java.util.HashSet<Historien>();
		}
		this.historien.add(henri);
	}

	public Historien getHistorien(String nom){
		int cpt = 0;
		Object[] tabHistorien = new Object[historien.size()];
		tabHistorien = historien.toArray();
		while (cpt < tabHistorien.length){
			if (((Historien) tabHistorien[cpt]).getNom().toLowerCase().equals(nom.toLowerCase())){
				return (Historien) tabHistorien[cpt];
			}
			else cpt = cpt+1;
		}
		return null;
	}

	public void addPhotographe(Photographe paul) {
		if (this.photographe == null) {
			this.photographe = new java.util.HashSet<Photographe>();
		}
		this.photographe.add(paul);
	}

	public Photographe getPhotographe(String nom){
		int cpt = 0;
		Object[] tabPhotographe = new Object[photographe.size()];
		tabPhotographe = photographe.toArray();
		while (cpt < tabPhotographe.length){
			if (((Photographe) tabPhotographe[cpt]).getNom().toLowerCase().equals(nom.toLowerCase())){
				return (Photographe) tabPhotographe[cpt];
			}
			else cpt = cpt+1;
		}
		return null;
	}

	public ArrayList<String> ConsulterDemandesModeration(Historien historien) {
		ArrayList<String> resultat = new ArrayList<String>();
		Historien histoTmp;
		Demande tmp;
		Collection<Demande> demandeSyst = this.getDemande();
		Iterator<Demande> iterateur = demandeSyst.iterator();
		for(int i = 0; i<demandeSyst.size(); i++){
			tmp = iterateur.next();
			histoTmp = tmp.getHistorien();
			if (histoTmp == historien && tmp.getEtat().getClass() == EtatEnModeration.class){
				resultat.add("Identifiant : " + tmp.getIdentifiant() + "\n" +
						"Description demande : " + tmp.getDescription() +
						"\n  Etat : " + tmp.getEtat());
			}
		}
		return resultat;
	}
}