/***********************************************************************
 * Module:  Etat.java
 * Author:  Flow
 * Purpose: Defines the Interface Etat
 ***********************************************************************/

import java.util.*;

/** @pdOid 41c3292c-e1c4-4e49-aee0-055c686640e7 */
public interface Etat {

   /** @pdOid 313114ec-ab05-47be-84d5-3b2fb5e894f9 */
   void valider();
   /** @pdOid 711b1e77-775c-427b-8b03-0577ca68ec50 */
   void prendre();
   /** @pdOid 2623fe05-7319-4d38-ac23-69c04bc70d78 */
   void moderer();
   /** @pdOid f35c16ad-ab9d-4a7d-bb06-34fabb8d6a77 */
   void abandonner();

}
