/***********************************************************************
 * Module:  EtatEnModeration.java
 * Author:  Flow
 * Purpose: Defines the Class EtatEnModeration
 ***********************************************************************/

/** @pdOid 370bebcb-63e1-4942-85cb-cfe86d2b1534 */
public class EtatEnModeration implements Etat {

    private Demande demande;

    public EtatEnModeration(Demande d){
        this.demande = d;
    }

    @Override
    public void valider() {
        demande.setEtat(new EtatValide(demande));
    }

    @Override
    public void prendre() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void moderer() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void abandonner() {

        throw new IllegalStateException("Action impossible avec l'état actuel");
    }
}