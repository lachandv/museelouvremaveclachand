/***********************************************************************
 * Module:  EtatEnAttente.java
 * Author:  Flow
 * Purpose: Defines the Class EtatEnAttente
 ***********************************************************************/

/** @pdOid 54dcebb7-b2d4-403e-9c12-82a1a3a310b8 */
public class EtatEnAttente implements Etat {

    private Demande demande;

    public EtatEnAttente(Demande d){
        this.demande = d;
    }

    @Override
    public void valider() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void prendre() {
        demande.setEtat(new EtatPrisEnCharge(demande));
    }

    @Override
    public void moderer() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void abandonner() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }
}