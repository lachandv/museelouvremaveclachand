/***********************************************************************
 * Module:  EtatPrisEnCharge.java
 * Author:  Flow
 * Purpose: Defines the Class EtatPrisEnCharge
 ***********************************************************************/

/** @pdOid 059cbcc8-7a04-40a4-b9bc-d39d08ca6dc2 */
public class EtatPrisEnCharge implements Etat {

    private Demande demande;

    public EtatPrisEnCharge(Demande d){
        this.demande = d;
    }

    @Override
    public void valider() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void prendre() {
        throw new IllegalStateException("Action impossible avec l'état actuel");
    }

    @Override
    public void moderer() {
        demande.setEtat(new EtatEnModeration(demande));
    }

    @Override
    public void abandonner() {
        demande.setEtat(new EtatEnAttente(demande));
    }
}