/***********************************************************************
 * Module:  Personnel.java
 * Author:  Flow
 * Purpose: Defines the Class Personnel
 ***********************************************************************/

import java.util.*;

/** @pdOid 382235c5-3ebb-43bb-840e-7c72b1515840 */
public class Personnel {
   /** @pdOid 9d8b8981-dfd1-4aed-ba46-e8ae26b7b5bf */
   private String nom;
   /** @pdOid a6573321-9a92-4049-9713-60bf3d677298 */
   private String prenom;
   /** @pdOid 976dcddb-e5ba-4ae4-80f7-7d475770c075 */
   private int identifiant;
   
   protected Systeme syst;
   
   private String mdp;

   public Personnel(String nom, String prenom, String mdp, Systeme syst){
	   this.nom = nom;
	   this.prenom = prenom;
	   this.mdp = mdp;
	   this.syst = syst;
   }
   
   /** @pdOid 37c4534d-c5aa-43e0-a6a9-995a34676202 */
   public Boolean authentification(String mdp) {
	   
      if (this.mdp.equals(mdp)){
    	  return true;
      }
      
      else return false;
   }

   public Systeme getSysteme() {
      return syst;
   }

   public String getNom() {
      return nom;
   }
}