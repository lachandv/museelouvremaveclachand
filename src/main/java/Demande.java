/***********************************************************************
 * Module:  Demande.java
 * Author:  Flow
 * Purpose: Defines the Class Demande
 ***********************************************************************/

import java.util.*;
import java.lang.Enum;

/** @pdOid a40038f1-c5af-438a-a00b-6294027f8dcc */
public class Demande {
   /** @pdOid 889d52ae-9d6c-478e-bd6f-516a5fac63bc */
   private String description;
   
   private int identifiant;
   
   private Etat etat;
   
   private Photographe photographe;
   
   private Historien historien;

   private static int idDemande;

   private String moderation;


   public Demande(String Description, Oeuvre oeuvre, Historien historien) {
	   this.description = Description;
	   this.identifiant = idDemande;
	   this.etat = new EtatEnAttente(this);
	   this.historien = historien;
	   this.photographe = null;
	   this.identifiant = idDemande;
       idDemande ++;
   }
   
   public Historien getHistorien() {
	   return this.historien;
   }
   
   public Photographe getPhotographe() {
	   return this.photographe;
   }
   
   public String getDescription() {
	   return this.description;
   }
   
   public int getIdentifiant() {
      return this.identifiant;
   }

   public Etat getEtat() {
	return this.etat;
}

   public void setEtat (Etat etat) throws IllegalStateException {
	this.etat = etat;
}

   public void setPhotographe(Photographe photographe) {
      this.photographe = photographe;
   }

   public void setModeration(String moderation) {
      this.moderation = moderation;
   }
}
