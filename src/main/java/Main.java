import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.UIManager;


public class Main {

	public static void main(String[] args) {
            
                    try {
    for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
            UIManager.setLookAndFeel(info.getClassName());
            break;
        }
    }
} catch (Exception e) {
    // If Nimbus is not available, you can set the GUI to another look and feel.
}
		// TODO Stub de la méthode généré automatiquement
		Oeuvre joconde = new Oeuvre();
		joconde.setNom("Joconde");
		joconde.setDescription("La joconde est un tableau de"
				+ " Léonard de Vinci, peint sur toile en lin avec de la"
				+ " jolie peinture.");
		Oeuvre tableau = new Oeuvre();
		tableau.setNom("Un joli tableau");
		tableau.setDescription("Ceci est un joli tableau.");
		
		Systeme louvre = new Systeme();
		louvre.addOeuvre(joconde);
		louvre.addOeuvre(tableau);


		
		Photographe paul = new Photographe("Paul","Paul","Paul", louvre);
		paul.authentification("Paul");
		
		Historien henri = new Historien("Henri","Henri","Henri", louvre);
		henri.authentification("Henri");
		louvre.addHistorien(henri);
		louvre.addPhotographe(paul);
		
		louvre.faireDemande("Demande 1", joconde, henri);
		louvre.faireDemande("Demande 2", tableau, henri);
                
                paul.prendreDemande(1);
                
                Fenetre fenetre = new Fenetre(louvre);
                fenetre.setVisible(true);


	}

}
