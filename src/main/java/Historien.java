/***********************************************************************
 * Module:  Historien.java
 * Author:  Flow
 * Purpose: Defines the Class Historien
 ***********************************************************************/

import java.util.*;
import java.util.Scanner;

/** @pdOid c174a03b-caf9-41a0-b864-23b290423b4e */
public class Historien extends Personnel {
   
   public Historien(String nom, String prenom, String mdp, Systeme syst){
	   super(nom, prenom, mdp, syst);
   }
   
   /** @pdOid 1c3811d4-6dee-44fe-813c-0d3bffb1b6ea */
   public void CreerFiche() {
      // TODO: implement
   }
   
   /** @pdOid e72a875c-279e-47e8-9531-b9c2a02db395 */
   public void ModifierFiche() {
      // TODO: implement
   }
   
   /** @pdOid 77770a5f-e188-4dea-869a-b58e6c22438a */
   public int creerDemande(String description, Oeuvre oeuvre, Historien historien) {
	   Demande demandeOeuvre = new Demande(description, oeuvre, historien);
      return demandeOeuvre.getIdentifiant();
   }
   
   /** @pdOid fc2dbfbb-e15f-48b6-b017-3cd14cb684fa */
   public ArrayList<String> consulterDemandes() {
	   return syst.ConsulterDemandesParHistorien(this);
   }


   public void creerDemande(String text, Oeuvre oeuvre) {
      syst.faireDemande(text, oeuvre, this);
   }

   public ArrayList<String> consulterDemandesModeration() {
      return syst.ConsulterDemandesModeration(this);
   }
}